package com.zuitt.dicussion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@SpringBootApplication
@RestController
//The RestController annotation tells spring boot that this application will function as an endpoint that will be used in handling web requests.

public class DicussionApplication {

	public static void main(String[] args) {
		SpringApplication.run(DicussionApplication.class, args);
	}


	@GetMapping("/hello")
	//Maps gets request to the route "/hello" and the method "hello()"
	public String hello () {
		return "Hello Ej!";
	}

	//Routes with a string query
	//Dynamic data is obtained form the URL`s string query
	//http://localhost:8080/hi?name=john
	@GetMapping("/hi")
	public String hi(@RequestParam(value = "name", defaultValue = "john") String name){
		return String.format("Hi %s",name);
		 }

	@GetMapping("/friend")
	public String friend(@RequestParam(value = "name", defaultValue ="Doe") String name,@RequestParam(value = "friend", defaultValue = "Jane") String friend){
		return String.format("Hello %s! My Name is %s.",friend,name);
	}

	//Activity s09

	ArrayList<String> enrollees = new ArrayList<String>();
	@GetMapping("/enroll")
	public String enrollUser(@RequestParam(value="user")String user){
		enrollees.add(user);
		return "Thank you for enrolling, "+user+"!";
	}
	@GetMapping("/getEnrollees")
	public String getEnrollees(){
		return enrollees.toString();
	}
	@GetMapping("/nameage")
	public String nameAge(@RequestParam(value="name")String name,@RequestParam(value="age") int age){
		return "Hello "+name+" My age is "+age;
	}
	@GetMapping("/courses/{id}")
	public String getCourse(@PathVariable String id){
		String courseName,schedule,price;

		if(id.equals("java101")){
			courseName = "java101";
			schedule = "MWF 8:00 AM - 11:00 AM";
			price = "PHP 3000";
		}

		else if(id.equals("sql101")){
			courseName = "sql101";
			schedule = "TTH 9:00 AM - 11:00 AM";
			price = "PHP 2500";
		}

		else if(id.equals("javaee101")){
			courseName = "javaee101";
			schedule = "MWF 1:30 PM - 4:00 PM";
			price = "PHP 4000";
		}

		else{
			return "That course cannot be found";
		}
		return "Name: "+ courseName+", "+"Schedule: "+schedule+", "+"Price: "+price;
	}




}
